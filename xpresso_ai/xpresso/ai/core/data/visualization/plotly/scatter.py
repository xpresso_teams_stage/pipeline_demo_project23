# Developed By Nalin Ahuja, nalin.ahuja@abzooba.com

import plotly.graph_objs as go
import numpy as np
from plotly.subplots import make_subplots
import os
from xpresso.ai.core.commons.utils.constants import EMPTY_STRING, DEFAULT_SUBPLOT_COLUMNS
import xpresso.ai.core.data.visualization.utils as utils
from xpresso.ai.core.data.visualization.plotly.res.plot_types import cartesian
from plotly.offline import init_notebook_mode


# End Imports-------------------------------------------------------------------------------------------------------------------------------------------------------------------

class new_plot(cartesian):
    def __init__(self, input_1, input_2, name=None, plot_title=None, output_format=utils.HTML, output_path=None,
                 auto_render=False,
                 filename=None,
                 axes_labels=None, auto_process=True):
        # Add Style to Plot
        self.style_plot()

        if axes_labels is None:
            axes_labels = dict()
            axes_labels["x_label"] = EMPTY_STRING
            axes_labels["y_label"] = EMPTY_STRING
        if plot_title is not None or filename is not None or axes_labels is not None:
            self.set_labels_manual(plot_label=plot_title, filename=filename)
            self.set_labels(plot_label=plot_title, x_axis_label=axes_labels["x_label"],
                            y_axis_label=axes_labels["y_label"])

        # Convert Inputs to Lists
        input_1 = (np.array(input_1).tolist())
        input_2 = (np.array(input_2).tolist())

        # Check Input Types
        if (type(input_1) is list and type(input_2) is list):
            self.x_axis_values = input_1
            self.y_axis_values = input_2

            # Return Error on Differing List Lengths
            if (not ((len(self.x_axis_values) == len(self.y_axis_values)))):
                print("ERROR: Differing list lengths!")
                utils.halt()

            # Process Set Arguments
            if (not (name is None)):
                self.set_name(name)
            if (not (output_path is None)):
                self.output_path = output_path
            if (auto_render):
                self.render(output_format=output_format, auto_open=True,
                            output_path=output_path)
            if (auto_process):
                self.process(output_format)
        else:
            # Return Error on Invalid Type
            print("ERROR: Invalid Input Type!")
            utils.halt()

    # End Object Constructor----------------------------------------------------------------------------------------------------------------------------------------------------

    def style_plot(self):
        # Apply Default Style to Plot
        self.apply_default_style()

        # Apply Custom Style to Plot
        self.view = "markers"
        self.marker = None

    # End Style Application Function--------------------------------------------------------------------------------------------------------------------------------------------

    def set_node_color(self, color=None):
        if (not (color is None)):
            color_arr = []

            # Set Color List with Specified Color
            for i in range(0, len(self.x_axis_values)):
                color_arr.append(color)

            # Set Marker with color_arr
            self.marker = dict(color=color_arr)

    # End Custom Style Functions------------------------------------------------------------------------------------------------------------------------------------------------

    def process(self, output_format):
        if output_format is utils.PNG or output_format is utils.PDF:
            # Generate Plot Layout and config for saved plots
            plot_data = go.Scatter(x=self.x_axis_values, y=self.y_axis_values,
                                   hovertemplate=self.hover_tool,
                                   marker=self.marker, name=self.graph_name,
                                   mode=self.view,
                                   text=self.y_axis_values, textposition='top left')
        else:
            # Generate Plot Config
            plot_data = go.Scatter(x=self.x_axis_values, y=self.y_axis_values,
                                   hovertemplate=self.hover_tool,
                                   marker=self.marker, name=self.graph_name,
                                   mode=self.view)

        # Error Check Axis Ranges
        self.check_ranges()

        # Generate Plot Layout
        plot_layout = go.Layout(autosize=True, width=self.plot_width,
                                height=self.plot_height,
                                paper_bgcolor=str(self.bg_color), plot_bgcolor=str(self.plot_color),
                                title=self.plot_title, xaxis=self.x_axis_title, yaxis=self.y_axis_title,
                                margin=go.layout.Margin(l=int(self.left), r=int(self.right), b=int(self.bottom),
                                                        t=int(self.top), pad=int(self.padding)))
        self.plot_data = plot_data
        return {'data': [plot_data], 'layout': plot_layout}

    def render(self, output_format=utils.HTML, output_path=None, auto_open=True):
        self._render_single_plot(__name__, output_format=output_format, output_path=output_path, auto_open=auto_open)

    # End Generation/Rendering Functions----------------------------------------------------------------------------------------------------------------------------------------


# End Graph Class---------------------------------------------------------------------------------------------------------------------------------------------------------------

class join(cartesian):
    def __init__(self, plots, output_format=utils.HTML, auto_render=True, filename=None, output_path=None,
                 plot_title=None):
        num_columns = DEFAULT_SUBPLOT_COLUMNS
        num_rows = int(len(plots) / num_columns) + 1
        self.new_plot = make_subplots(rows=num_rows, cols=num_columns)
        # set the layoput of combined subplot with height and width =900
        self.new_plot.update_layout(height=900, width=900, showlegend=False, title_text=plot_title)
        for plot in plots:
            index = plots.index(plot)
            row = int(index / num_columns) + 1
            column = index % num_columns + 1
            self.new_plot.add_trace(plot.plot_data, row=row, col=column)
            self.new_plot.update_xaxes(title_text=str(plot.x_axis_title["title"]["text"]), row=row, col=column)
            self.new_plot.update_yaxes(title_text=str(plot.y_axis_title["title"]["text"]), row=row, col=column)

        if auto_render:
            self.render(output_format=output_format, filename=filename, output_path=output_path)

    def render(self, output_format=utils.HTML, output_path=None, auto_open=True, filename=None):

        if not filename:
            filename = "scatter_plot"
        # Render Appropriate Format
        if (output_format == utils.PNG):
            out_folder = utils.DEFAULT_IMAGE_PATH
            if output_path is not None:
                out_folder = output_path

            utils.make_container(out_folder)

            output_name = filename + "_" + utils.generate_id() + ".png"
            self.new_plot.write_image(os.path.join(out_folder, str(output_name)))

        elif (output_format == utils.PDF):

            out_folder = utils.DEFAULT_PDF_PATH
            if output_path is not None:
                out_folder = output_path

            utils.make_container(out_folder)

            output_name = filename + "_" + utils.generate_id() + ".pdf"
            self.new_plot.write_image(os.path.join(out_folder, str(output_name)))

        elif (output_format == utils.HTML):

            out_folder = utils.DEFAULT_PLOT_PATH
            if output_path is not None:
                out_folder = output_path

            # Render Plot
            if (utils.detect_notebook()):
                # Initialize Notebook Mode
                init_notebook_mode(connected=True)
                utils.clear_terminal()

                # Render Notebook Plot
                output_name = "notebook_" + str("scatter_plot") + "_" + \
                              utils.generate_id() + ".html"
                self.new_plot.show()

            else:
                # Render Local Plot
                utils.make_container(out_folder)
                import plotly as plotly
                output_name = filename + "_" + utils.generate_id() + ".html"
                plotly.offline.plot(self.new_plot, filename=os.path.join(out_folder, str(output_name)))
                return os.path.join(
                    out_folder, str(output_name))

        else:
            # Return Error on Invalid Output Format
            print("ERROR: Invalid Output Format!")
            utils.halt()
