""" Class definition of automl Info """

from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.exploration.attribute_info import AttributeInfo
from xpresso.ai.core.data.exploration.explore_files import ExploreFiles
from xpresso.ai.core.data.exploration.explore_text import ExploreText
from xpresso.ai.core.commons.utils.constants import FILE_PATH_COL, FILE_SIZE_COl
import os

__all__ = ['UnstructuredDatasetInfo']
__author__ = 'Srijan Sharma'

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class UnstructuredDatasetInfo:
    """ DatasetInfo contains the detailed information about the
    automl. This information contains the attribute list,
    attribute type, attribute metrics"""

    def __init__(self):
        self.attributeInfo = list()
        self.metrics = dict()
        return

    def understand_attributes(self, data, dataset_type: DatasetType.UTEXT):
        """Populates the filesize distribution

        Args:
            filename (str): filename of the distribution plot for attribute
            input_path (str): path to the folder containing the plot for
            attribute
            table (:obj: `dict`): description data for attribute
        Returns:
        """

        if not isinstance(dataset_type, DatasetType):
            logger.warning("Datatype not supported for "
                           "method(understand_attributes) of "
                           "UnstructuredDatasetInfo ")
            return

        attr_name = "Size"
        size_col_name = FILE_SIZE_COl
        self.attributeInfo = AttributeInfo(attr_name)
        if size_col_name not in data.columns:
            logger.warning("File size information not found in the provided "
                           "DataFrame")
            return
        self.attributeInfo.metrics = ExploreFiles(data[
                                                      size_col_name]).populate_filesize()

        return

    def populate_unstructured(self, data, type):
        """Populates the metric for the Unstructured Dataset
        Args:
             data(:dataframe): Dataframe with first attribute having filepath
             and second having the filesize
             type(:string): either DatasetType.UTEXT or DatasetType.UIMAGE
         """

        if not len(data):
            logger.warning("Unable to perform Unstructured  text data "
                           "analytics. No files present in the dataframe")

        if type is DatasetType.UTEXT:
            file_path = FILE_PATH_COL
            for index, row in data.iterrows():
                text_file_abspath = os.path.abspath(os.path.join(
                    os.getcwd(), row[file_path]))

                try:
                    text_data = open(text_file_abspath, "r").read()
                    self.populate_ngram(text_data)
                except Exception:
                    logger.error(
                        "Unable to read file {}".format(text_file_abspath))

        elif type is DatasetType.UIMAGE:
            print("Unstructured Image type not supported")
            logger.warning("Unstructured Image type not supported")
        return

    def populate_ngram(self, text_data):
        ngram_analysis = ExploreText(text_data).populate_text()
        for ngram in ngram_analysis.keys():

            if ngram not in self.metrics.keys():
                self.metrics[ngram] = dict()

            for ngram_value, ngram_count in ngram_analysis[
                ngram].items():
                if ngram_value not in self.metrics[ngram].keys():
                    self.metrics[ngram][ngram_value] = ngram_count
                else:
                    self.metrics[ngram][ngram_value] = self.metrics[
                                                           ngram][
                                                           ngram_value] + ngram_count
        return
