#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Building xpresso dependencies
IFS=',' read -ra xpresso_dependencies_list <<< "${XPRESSO_DEPENDENCIES}"
for dep in "${xpresso_dependencies_list[@]}"; do
    cd ${ROOT_FOLDER}/../${dep}
    make install
done


# Build the dependencies
pip install -r ${ROOT_FOLDER}/requirements/requirements.txt
